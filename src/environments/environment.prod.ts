export const environment = {
  production: true,
  apiBaseUrl: '/api/v1',
  diagnoseApiBaseUrl: 'https://virtserver.swaggerhub.com/twohat/classifyText/2.0.1',
  sentry: {
    dsn: 'https://1f168fd1b0fa4b6999f1a901c0804a91@sentry.io/1788710',
    environment: 'production',
    ignoreErrors: [
      'Non-Error exception captured'
    ]
  }
};
