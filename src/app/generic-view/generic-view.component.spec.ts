import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericViewComponent } from './generic-view.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ClassificationService } from './services/classification.service';
import { DefaultService } from 'sift-swagger-client';
import { SharedComponentsModule } from '../shared-components/shared-components.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
// import { BreadcrumbsComponent } from '../shared-components/breadcrumbs/breadcrumbs.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('GenericViewComponent', () => {
  let component: GenericViewComponent;
  let fixture: ComponentFixture<GenericViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule, SharedComponentsModule, HttpClientTestingModule ],
      declarations: [ GenericViewComponent ],
      providers: [ ClassificationService, DefaultService ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
