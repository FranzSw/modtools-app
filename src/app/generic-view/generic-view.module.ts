import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadcrumbModule } from 'xng-breadcrumb';

import { GenericViewComponent } from './generic-view.component';
import { GenericViewRoutingModule } from './generic-view-routing.module';
import { SharedComponentsModule } from '../shared-components/shared-components.module';
import { WordGroupComponent } from './word-group/word-group.component';
import { FindValuePipe } from './pipes/find-value.pipe';
import { TokenListComponent } from './token-list/token-list.component';


@NgModule({
  declarations: [GenericViewComponent, WordGroupComponent, FindValuePipe, TokenListComponent],
  imports: [
    CommonModule,
    GenericViewRoutingModule,
    SharedComponentsModule,
    BreadcrumbModule,
  ]
})
export class GenericViewModule { }
