import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TextClassifiedOutput, TextInput } from 'sift-swagger-client';
import { Observable } from 'rxjs';
import { ClassificationService } from './services/classification.service';
import { Languages } from 'src/constants';

@Component({
  selector: 'app-generic-view',
  templateUrl: './generic-view.component.html',
  styleUrls: ['./generic-view.component.less']
})
export class GenericViewComponent implements OnInit {

  // Observable resulting in classification from API
  classification$: Observable<TextClassifiedOutput>;

  // Variables for state
  href: string;
  search: string;
  selectedClientId = 60;
  selectedContentType: TextInput.ContentTypeEnum = 'SHORT_TEXT';
  selectedLanguage = 'en';
  extended = false;

  // Variables needed for template
  languages = Languages;
  // TODO move these elsewhere, but I don't know where this data will come from / if its constant:
  clients = [
    {id: 60, name: 'Live'},
    {id: 61, name: 'Sandbox'}
  ];
  contentTypes: { id: TextInput.ContentTypeEnum, name: string }[] = [
    {id: 'SHORT_TEXT', name: 'Chat'},
    {id: 'LONG_TEXT', name: 'Text'},
    {id: 'USERNAME', name: 'Username'},
  ];
  policies= [
    {name: 'Global Chat', thresholds: {
      0: 6,
      5: 5
    }},
    {name: 'Private Chat', thresholds: {
      5: 6
    }}
  ];

  constructor(
    private route: ActivatedRoute,
    private classificationService: ClassificationService,
  ) { }

  ngOnInit(): void {
    // Subscribe to route changes and update search, url and classification on change
    this.route.paramMap.subscribe((params) => {
      this.search = params.get('search');
      this.href = window.location.href;

      this.getClassification();
    });

  }

 /**
  * Calls classificationService and updates classification$-variable
  */
  getClassification(){
    const args: TextInput = {
      text: this.search,
      clientId: this.selectedClientId,
      language: this.selectedLanguage,
      contentType: this.selectedContentType
    }

    this.classification$ = this.classificationService.getClassification(args, this.extended);
  }

  // Handlers for input changes. Sets local variable according to selection
  handleLanguageChange(event: Event){
    this.selectedLanguage = (event.target as HTMLInputElement).value;
  }
  handleContentTypeChange(event: Event){
    this.selectedContentType = (event.target as HTMLInputElement).value as TextInput.ContentTypeEnum;
  }
  handleClientChange(event: Event){
    this.selectedClientId = Number((event.target as HTMLInputElement).value);
  }
  handleExtendedClick(event: Event){
    this.extended = (event.target as HTMLInputElement).checked;
  }

}
