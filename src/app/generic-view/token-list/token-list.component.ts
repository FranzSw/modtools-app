import { Component, OnInit, Input } from '@angular/core';
import { Rule } from 'sift-swagger-client/model/rule';

@Component({
  selector: 'token-list',
  templateUrl: './token-list.component.html',
  styleUrls: ['./token-list.component.less']
})
export class TokenListComponent implements OnInit {
  @Input() tokens: Rule[];

  constructor() { }

  ngOnInit(): void {
  }

}
