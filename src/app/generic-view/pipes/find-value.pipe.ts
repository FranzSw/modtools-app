import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'findValue'
})
export class FindValuePipe implements PipeTransform {

  transform(object: {[key: string]: number}|{[key: number]: number}, action: 'max'|'min', ...args: unknown[]): number|typeof object {
    if(object !== Object(object)) return object;

    if(action==='max')
      return Math.max(...Object.values(object));
    else if(action==='min')
      return Math.max(...Object.values(object));
    else
      return 0;
  }

}
