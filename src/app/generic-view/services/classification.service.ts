import { Injectable } from '@angular/core';
import { TextInput, DefaultService, Slots } from 'sift-swagger-client';
import { isEqual } from 'lodash';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ClassificationService {

  constructor(
      private api: DefaultService,
    ) { }

  /**
   * Sends a classification request to API
   * @param text Text to get classification for
   * @param language selected language
   * @param clientId selected clientId
   */
  getClassification(textInput: TextInput, extended?: boolean){
    return this.api.classifyText(textInput, extended).pipe(
      tap(classification => {
        // group extended-tokens and find best token
        classification.extended = this._groupAndFlattenWords(classification.extended);
        classification.extended.forEach(slot => this._findBestToken(slot));
        return classification;
      })
    );
  }

  /**
   * Groups slots based on if their text is empty and the solution
   * is the same as the previous slot's solution.
   * @param slots slots to group
   * @returns A new array of slots
   */
  private _groupAndFlattenWords(slots: Slots[]){
    const result: Slots[] = [];

    let groupIndex = 0;
    // Setup array with first slot from slots
    const [firstSlot, ...remaining] = slots;
    result[0] = firstSlot;

    remaining.forEach(slot => {
      // Check if slot belongs to current group
      if(slot.text === '' && result[groupIndex] && result[groupIndex].solution === slot.solution)
        this._mergeWords(result[groupIndex], slot);
      else
        // If not store slot at next index
        result[++groupIndex] = slot;
    });

    return result;
  }

  /**
   * Concats original-text & merges tokens
   * @param resultSlot The slot on which is operated
   * @param slotToMerge The slot that should be merged into the resultSlot
   */
  private _mergeWords(resultSlot: Slots, slotToMerge: Slots){
    resultSlot.original += ` ${slotToMerge.original}`;
    // Text & solution is the same in both, so no need to copy

    // Loop over tokens that should get merged and check for duplicates
    slotToMerge.tokens.forEach(token => {
      // Only check text first, because deep comparison is expensive.
      const tokenWithSameText = resultSlot.tokens.find(existingToken => existingToken.text === token.text);

      if(!tokenWithSameText || !isEqual(token, tokenWithSameText))
        resultSlot.tokens.push(token);
    });

    return resultSlot;
  }


  /**
   * Finds first token which text matches the solution and pushes it to the front
   * @param slot Slot to find best token in
   */
  private _findBestToken(slot: Slots){
    const bestTokenIndex = slot.tokens.findIndex(token => token.text === slot.solution);
    // Cut element out of array
    const bestToken = slot.tokens.splice(bestTokenIndex, 1)[0];
    // Insert at first position
    slot.tokens.splice(0, 0, bestToken);
  }
}
