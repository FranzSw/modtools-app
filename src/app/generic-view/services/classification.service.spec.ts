import { TestBed } from '@angular/core/testing';

import { ClassificationService } from './classification.service';
import { DefaultService } from 'sift-swagger-client';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ClassificationService', () => {
  let service: ClassificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ DefaultService ]
    });
    service = TestBed.inject(ClassificationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
