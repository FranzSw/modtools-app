import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FindValuePipe } from '../pipes/find-value.pipe';
import { TokenListComponent } from '../token-list/token-list.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { WordGroupComponent } from './word-group.component';

describe('WordGroupComponent', () => {
  let component: WordGroupComponent;
  let fixture: ComponentFixture<WordGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WordGroupComponent, FindValuePipe ],
      providers: [ FindValuePipe, TokenListComponent ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WordGroupComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    component.wordgroup = {
      original: '',
      solution: '',
      tokens: [
        {
          topics: {0: 0}
        }
      ]
    };

    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
});
