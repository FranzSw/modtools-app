import { Component, OnInit, Input } from '@angular/core';
import { Slots } from 'sift-swagger-client';

@Component({
  selector: 'word-group',
  templateUrl: './word-group.component.html',
  styleUrls: ['./word-group.component.less']
})
export class WordGroupComponent implements OnInit {
  @Input() wordgroup: Slots;

  constructor() { }

  ngOnInit(): void {  }

}
