import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'main-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.less']
})
export class SearchComponent implements OnInit {

  @Output() userSearched = new EventEmitter<string>();

  constructor(private router: Router) { }

  ngOnInit() {
  }

  handleKeyDown(e: KeyboardEvent){
    const inputValue = (e.target as HTMLInputElement).value;

    if(e.key === 'Enter' && inputValue !== ''){
      this.userSearched.emit(inputValue);
    }
  }

}
