import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-toggle-button',
  templateUrl: './toggle-button.component.html',
  styleUrls: ['./toggle-button.component.less']
})
export class ToggleButtonComponent implements OnInit {

  @Input() name: string;
  @Output() toggleChange = new EventEmitter<Event>();

  id: string;

  constructor() { }

  handleChange(ev: Event){
    this.toggleChange.emit(ev);
  }

  ngOnInit(): void {
    // generate a random id to connect label and input
    this.id = '_' + Math.random().toString(36).substr(2, 9);
  }

}
