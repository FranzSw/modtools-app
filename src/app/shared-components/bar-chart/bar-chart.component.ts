import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.less']
})
export class BarChartComponent implements OnInit {
  @Input() barData: {[key: string]: number};

  labels: string[];
  displayValues: {[key: string]: string};
  constructor() { }

  ngOnInit(): void {
    this.labels = Object.keys(this.barData);
    this.displayValues = this.labels.reduce((currentResult, label) => {
      return {...currentResult, [label]: Math.round(this.barData[label]*100) + '%'  }
    }, {})
  }

}
