import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { TopicChipComponent } from './topic-chip.component';
import { Topics } from 'src/constants';

describe('TopicChipComponent', () => {
  let component: TopicChipComponent;
  let fixture: ComponentFixture<TopicChipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopicChipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicChipComponent);
    component = fixture.componentInstance;
    component.topicId = 0;
    component.riskLevel = 5;
    component.small = false;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have risk border class', () => {
    const riskToCheck = [4, 6, 8];

    riskToCheck.forEach(riskLevel => {
      component.riskLevel = riskLevel;
      fixture.detectChanges();

      const chip = fixture.debugElement.query(By.css('.topic-chip'));
      expect(chip.classes[`border-risk-${riskLevel}`]).toBeTruthy();
    })
  });

  it('should show topic icon and name', () => {
    const topics = [0, 5];

    topics.forEach(topicId => {
      component.topicId = topicId;
      const topic = Topics[topicId];

      fixture.detectChanges();

      const nameElement = fixture.debugElement.query(By.css('.name'));
      expect(nameElement.nativeElement.textContent.trim()).toBe(topic.name);

      const icon = fixture.debugElement.query(By.css('.mdi'));
      expect(icon.classes[`mdi-${topic.icon}`]).toBeTruthy();
    })
  })

  it('should hide name if small', () => {
    component.small = true;

    fixture.detectChanges();

    const nameElement = fixture.debugElement.query(By.css('.name'));
    expect(nameElement).toBeNull();
  });
});
