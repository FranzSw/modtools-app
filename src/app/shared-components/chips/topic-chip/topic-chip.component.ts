import { Component, OnInit, Input } from '@angular/core';
import { Topics } from 'src/constants';

@Component({
  selector: 'topic-chip',
  templateUrl: './topic-chip.component.html',
  styleUrls: ['./topic-chip.component.less']
})
export class TopicChipComponent implements OnInit {

  @Input() topicId: number;
  @Input() riskLevel: number;
  @Input() small = false;

  topicData: {name: string; icon: string};

  constructor() { }

  ngOnInit(): void {
  }

  get topicName(){
    return Topics[this.topicId].name;
  }
  get topicIcon(){
    return Topics[this.topicId].icon
  }

}
