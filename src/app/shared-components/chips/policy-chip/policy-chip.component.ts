import { Component, OnInit, Input } from '@angular/core';
import { Topics } from 'sift-swagger-client/model/topics';

@Component({
  selector: 'policy-chip',
  templateUrl: './policy-chip.component.html',
  styleUrls: ['./policy-chip.component.less']
})
export class PolicyChipComponent implements OnInit {
  @Input() policy: { name: string; thresholds: Topics};
  @Input() topics: Topics;

  name: string;
  pass = true;

  constructor() { }

  ngOnInit(): void {
    this.name = this.policy.name;
    Object.keys(this.policy.thresholds).forEach(topicId => {
      const topic = this.topics[topicId];
      const policyThreshold = this.policy.thresholds[topicId];

      if(topic !== undefined && topic >= policyThreshold)
        this.pass = false;
    });
  }

}
