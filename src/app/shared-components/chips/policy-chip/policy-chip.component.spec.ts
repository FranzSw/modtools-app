import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolicyChipComponent } from './policy-chip.component';

describe('PolicyChipComponent', () => {
  let component: PolicyChipComponent;
  let fixture: ComponentFixture<PolicyChipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolicyChipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolicyChipComponent);
    component = fixture.componentInstance;
    component.policy = {name: '', thresholds: []};
    component.topics = {};

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
