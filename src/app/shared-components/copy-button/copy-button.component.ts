import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'copy-button',
  templateUrl: './copy-button.component.html',
  styleUrls: ['./copy-button.component.less']
})
export class CopyButtonComponent implements OnInit {

  @Input() toCopy: string;
  @Input() title: string;

  success = false;

  constructor() { }

  ngOnInit(): void {
  }

  handleClick(){
    this._copyText(this.toCopy);
    this.success = true;
    setTimeout(() => this.success=false, 1000);
  }

  private _copyText(text: string){
    const element = document.createElement('textarea');
    element.value = text;
    // element.setAttribute('readonly', '');
    element.style.position = 'absolute';
    element.style.left = '-9999px';
    document.body.appendChild(element);
    element.select();
    document.execCommand('copy');
    document.body.removeChild(element);
  };
}
