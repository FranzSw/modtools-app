import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { AppGridComponent } from './app-grid/app-grid.component';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { LoadingIndicatorComponent } from './loading-indicator/loading-indicator.component';
import { MoreButtonComponent } from './more-button/more-button.component';
import { CopyButtonComponent } from './copy-button/copy-button.component';
import { OptionButtonGroupComponent } from './option-button-group/option-button-group.component';
import { BarChartComponent } from './bar-chart/bar-chart.component';
import { ToggleButtonComponent } from './toggle-button/toggle-button.component';
import { TopicChipComponent } from './chips/topic-chip/topic-chip.component';
import { PolicyChipComponent } from './chips/policy-chip/policy-chip.component';

@NgModule({
  declarations: [
    AppGridComponent,
    BreadcrumbsComponent,
    LoadingIndicatorComponent,
    MoreButtonComponent,
    CopyButtonComponent,
    OptionButtonGroupComponent,
    TopicChipComponent,
    PolicyChipComponent,
    BarChartComponent,
    ToggleButtonComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
  ],
  exports: [
    AppGridComponent,
    BreadcrumbsComponent,
    LoadingIndicatorComponent,
    MoreButtonComponent,
    CopyButtonComponent,
    OptionButtonGroupComponent,
    TopicChipComponent,
    PolicyChipComponent,
    BarChartComponent,
    ToggleButtonComponent,
  ]
})
export class SharedComponentsModule { }
