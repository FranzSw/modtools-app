import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'filter-quality/audit-rules'
  },
  {
    path: 'filter-quality',
    data: { breadcrumb: 'Filter Quality' },
    children: [
      {
        path: 'audit-rules',
        data: { breadcrumb: 'Audit Rules' },
        loadChildren: () => import('./generic-view/generic-view.module').then(m => m.GenericViewModule),
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
