# ModTools

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.4.

## Contributing Guidelines

See [CONTRIBUTING.md](CONTRIBUTING.md) for information on contributing to our projects.

## Setup

Run `npm run setup` to build the sift-swagger-client (from projects/sift-swagger-client) and link it to the project. 
On Linux run `sudo npm run setup` (see below).

The npm-script runs the following commands:
```
ng build sift-swagger-client
cd projects/sift-swagger-client/dist
npm link
cd ../../..
npm link sift-swagger-client
```

### Permission Error:
If the script fails with an `permission denied` error after building, you can either run the script as root (`sudo npm run setup`) or run only the `npm link` steps as root user / with administrator rights (`sudo npm link`, ...).

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. Use `npm run build:prod` for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
